<?php
class Paginator {
     
     private $_db;
 
     function __construct()
     {
         $this->_db = Database::getInstance();
     }
 
     public function dataview($query)
     {
         $stmt = $this->_db->_pdo->prepare($query);
         $stmt->execute();
    ?>     
	
	<table id='tfhover' class='tftable' border='1'>
         <tr>
           <th>id</th>
           <th>Username</th>
            <th>Password</th>
            <th>First Name</th>
            <th>Surname</th>
            <th>Permission</th>
            <th>Delete</th>
            <th>Update</th>
        </tr>
	<?php 
         if($stmt->rowCount()>0)
         {
                while($row=$stmt->fetch(PDO::FETCH_ASSOC))
                {
                   ?>
                   <tr>
                   <td><?php echo $row['id']; ?></td>
                   <td><?php echo $row['username']; ?></td>
                   <td><?php echo $row['password']; ?></td>
                   <td><?php echo $row['firstname']; ?></td>
                   <td><?php echo $row['surname']; ?></td>
                   <td><?php echo $row['permission']; ?></td>
                   <td><a id="link_button" href="admin_delete.php?id=<?php echo $row['id']; ?>" ><input type="button" value="Delete" name="delete" ><input class="hidden" name="delete_id" type="hidden"  value="<?php echo $row['id']; ?>"> </a></td>
                   <td><a id="link_button"href="admin_update.php?id=<?php echo $row['id']; ?>"><input type="button" value="Update" name="update" ></a><input class="hidden" type="hidden" name="udate_id"  value="<?php echo $row['id']; ?>" ></td>
                   </tr>
                   <?php
                }
         }
         else
         {
                ?>
                <tr>
                <td>Nothing here...</td>
                </tr>
                <?php
         }
  
 }
 
 public function paging($query,$records_per_page)
 {
        $starting_position=0;
        if(isset($_GET["page_no"]))
        {
             $starting_position=($_GET["page_no"]-1)*$records_per_page;
        }
        $query2=$query." limit $starting_position,$records_per_page";
        return $query2;
 }
 
 public function paginglink($query,$records_per_page)
 {
  
        $self = $_SERVER['PHP_SELF'];
  
        $stmt = $this->_db->_pdo->prepare($query);
        $stmt->execute();
  
        $total_no_of_records = $stmt->rowCount();
  
        if($total_no_of_records > 0)
        {
            ?><tr><td colspan="3"><?php
            $total_no_of_pages=ceil($total_no_of_records/$records_per_page);
            $current_page=1;
            if(isset($_GET["page_no"]))
            {
               $current_page=$_GET["page_no"];
            }
            if($current_page!=1)
            {
               $previous =$current_page-1;
               echo "<a href='".$self."?page_no=1'>First</a>&nbsp;&nbsp;";
               echo "<a href='".$self."?page_no=".$previous."'>Previous</a>&nbsp;&nbsp;";
            }
            for($i=1;$i<=$total_no_of_pages;$i++)
            {
            if($i==$current_page)
            {
                echo "<strong><a href='".$self."?page_no=".$i."' style='color:red;text-decoration:none'>".$i."</a></strong>&nbsp;&nbsp;";
            }
            else
            {
                echo "<a href='".$self."?page_no=".$i."'>".$i."</a>&nbsp;&nbsp;";
            }
   }
   if($current_page!=$total_no_of_pages)
   {
        $next=$current_page+1;
        echo "<a href='".$self."?page_no=".$next."'>Next</a>&nbsp;&nbsp;";
        echo "<a href='".$self."?page_no=".$total_no_of_pages."'>Last</a>&nbsp;&nbsp;";
   }
   ?></td></tr><?php
  }
 }
}