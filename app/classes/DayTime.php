<?php
class DayTime {


	public static function time() {

		date_default_timezone_set('Europe/Belgrade');
		$hour = date('H');

		if ($hour >= Config::get('time/day') && $hour <= Config::get('time/night')) {	
			 	return "day";
			 	//return true;
		} else {
		 	return "night";
		 	
		}
	}

	public function timenow() {
		return date('l jS \of F Y h:i:s A');
	}


}

?>