<?php

	session_start();
	$GLOBALS['config'] = array(
		'mysql'		=> array(
			'host'		=> '127.0.0.1',
			'username'	=> 'root',
			'password'	=> 'password',
			'db'		=> 'vezba1',
		),
		'remember'	=> array(
			'cookieName'	=> 'hash',
			'cookieExpiry'	=> 604800,	//1 Week
		),
		'session'	=> array(
			'sessionName'	=> 'user',
		),
		'paginator' => array(
			'results'	 => 10 //choose users per page
		),
		'time' => array(	//choose hours for day and night
			'day'   => 6,
			'night' => 18,
		),
	);
	
	spl_autoload_register(function($class) {
		require_once 'app/classes/'.$class.'.php';
	});

	 require_once 'app/functions/sanitize.php';


?>