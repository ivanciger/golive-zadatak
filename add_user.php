<?php 
	require_once 'app/core/init.php';

	$user = new User();

	if ($user->isLoggedIn()) {

		if (Input::exists()) {
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'username'	=> array(
					'fieldName'	=> 'Username',
					'required' 	=> true,
					'min'		=> 2,
					'max'		=> 20,
					'unique'	=> 'users'
				),
				'password'	=> array(
					'fieldName'	=> 'Password',
					'required' 	=> true,
					'min'		=> 6
				),
				'passwordAgain' => array(
					'fieldName'	=> 'Password Repeat',
					'required' 	=> true,
					'min'		=> 6,
					'matches'	=> 'password'
				),
				'firstname'	=> array(
					'fieldName'	=> 'First name',
					'required' 	=> true,
					'min'		=> 2,
					'max'		=> 50
				),
				'surname'	=> array(
					'fieldName'	=> 'Surname',
					'required' 	=> true,
					'min'		=> 2,
					'max'		=> 50
				),
				'permission'	=> array(
					'required'	=> true
				)		
			));

			if ($validation->passed()) {
				$newuser = new User();
				$salt = Hash::salt(32);
				try {
					$newuser->create(array(
						'username' 		=> Input::get('username'),
						'password' 		=> Hash::make(Input::get('password'),$salt),
						'salt' 			=> $salt,
						'firstname'		=> Input::get('firstname'),
						'surname' 		=> Input::get('surname'),
						'permission'	=> Input::get('permission')
					));
					Session::flash('home','User have been registered !');
					Redirect::to('admin.php');
				} catch (Exception $e) {
					die($e->getMessage());
				}
			} else {
				foreach ($validation->errors() as $error) {
					echo $error, '<br>';
				}
			}
		}
	


?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/<?php echo DayTime::time(); ?>.css" >
</head>
<body>
<form action="" method="post">
	<div class="field">
		<label for="username">Username</label>
		<input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off"/>
	</div>
	<div class="field">
		<label for="password">Password</label>
		<input type="password" name="password" id="password"/>
	</div>
	<div class="field">
		<label for="password_again">Enter your password again</label>
		<input type="password" name="passwordAgain" id="passwordAgain"/>
	</div>
	<div class="field">
		<label for="firstname">First Name</label>
		<input type="text" name="firstname" id="firstname" value="<?php echo escape(Input::get('firstname')); ?>"/>
	</div>
	<div class="field">
		<label for="surname">Surname</label>
		<input type="text" name="surname" id="surname" value="<?php echo escape(Input::get('surname')); ?>"/>
	</div>
	<div class="field">
		<label for="permission">Permission</label>
		<input type="radio" name="permission" value="1">admin
		<input type="radio" name="permission" value="2">user<br><br>
	</div>
	<input type="submit" value="Add User"/>
</form>




<?php
	} else {
		echo "<p>Only for admins<a href='login.php'>login</a> </p>";
	}
?>

</body>
</html>