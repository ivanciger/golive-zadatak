<?php
	require_once 'app/core/init.php';
	
$count=count($_POST);
if ($count<=3) { 
// check without captcha	
if (Input::exists()) {		
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'username'	=> array(
					'fieldName'	=> 'Username',
					'required' 	=> true
				),
				'password'	=> array(
					'fieldName'	=> 'Password',
					'required' 	=> true
				)
			));

			if ($validation->passed()) {
				$user 		= new User();
				$login 		= $user->login(Input::get('username'),Input::get('password'));

				if ($login) {
					if ($user->data()->permission == 1) {
						Redirect::to('admin.php');
					} else {
						Redirect::to('index.php');
						break;
					  }				
				
				} else {
					echo "Sorry we could not log you in";
				}
			} else {
				foreach ($validation->errors() as $error) {
					echo $error, '<br>';
				}
			}
	}
} else {
// with captcha
	if (Input::exists()) {		
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'username'	=> array(
					'fieldName'	=> 'Username',
					'required' 	=> true
				),
				'password'	=> array(
					'fieldName'	=> 'Password',
					'required' 	=> true
				),
				'captcha' => array(
					'fieldName' => 'Captcha',
					'required' => true
				)
			));

			if ($validation->passed()) {
				
					$user 		= new User();
					$login 		= $user->login(Input::get('username'),Input::get('password'));

					if ($login) {
						if ($_SESSION["captcha"] == Input::get('captcha')) { 	
							if ($user->data()->permission == 1) {
								Redirect::to('admin.php');
							} else {
								Redirect::to('index.php');
								break;
							  }				
						} else {
							echo 'Captcha not valid';
						}	
					} else {
						echo "Inputs are wrong";
					}
				
			} else {
				foreach ($validation->errors() as $error) {
					echo $error, '<br>';
				}
			}
	}
}	

		$_SESSION['captcha']="hidden";
		if(isset($_POST['count_text'])){
		@$_SESSION['count_text']=$_POST['count_text'] +$_SESSION['count_text'];
		$count_text=$_SESSION['count_text'];
		echo $count_text;
			if($count_text>1){
			$_SESSION['captcha']="visible";
			}
		}


		?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/login.css" > 
</head>
<body >

<form id="content" action="" method="post">

	<div class="field">
		<label for="username">Username</label>
		<input type="text" name="username" id="username" value="<?php  echo escape(Input::get('username'));  ?>" autocomplete="off"/>
	
	<br>
	<div class="field">
		<label for="passsword">Password</label>
		<input type="password" name="password" id="password"/>
	</div>
	<br>
	<div id="<?php echo $_SESSION['captcha']; ?>">
		<p id="numbers"> 
			<?php  if(isset($_SESSION['captcha'])&&$_SESSION['captcha']=="visible") { ?>
				<label for="captcha">Enter image text</label>	<br>
				<img id="captcha" src="app/captcha/captcha.php" alt="CAPTCHA Image" />
				<input name="captcha" type="text" size="4"> 
				<?php } ?> 
		</p>  
	</div>
	<input id="count_text" type="text" value="1" name="count_text" >
	<br>
	<input type="submit" value="Login"/>
</form>


</body>
</html>

