<?php 
	require_once 'app/core/init.php';

	if (Session::exists('home')) {
		echo Session::flash('home');
	} 

	$user = new User();

?>
	
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/<?php echo DayTime::time(); ?>.css" >
</head>
<body>
<?php
if ($user->isLoggedIn()) {
?>
	<p>Welcome <?php echo escape($user->data()->firstname.' '.$user->data()->surname); ?></p>
	<ul>
		<li><a href="changepassword.php">Change Password</a></li>
		<li><a href="logout.php">Logout</a></li>
	</ul>

	

	<form id="content" action="webservis.php" method="get">
	<input type="hidden" name="username" id="username" value="<?php  echo escape($user->data()->username);  ?>" autocomplete="off"/>
	<input type="submit" value="Web Servis"/>
	</form>
<?php
	} else {
		echo "<p>You need to <a href='login.php'>login</a> </p>";
	}
	
?>



</body>
</html>
