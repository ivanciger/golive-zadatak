<?php
    require_once 'app/core/init.php';

       $user = new User();
    if ($user->isLoggedIn() && ($user->data()->permission == 1)) {

 
    $paginate = new Paginator();
?>
<link rel="stylesheet" type="text/css" href="css/<?php echo DayTime::time(); ?>.css" >
<table align="center" width="50%"  border="1">
<tr>
<td>List of all users</td>
</tr>
<tr>
<td>

        <table align="center" border="1" width="100%" height="100%" id="data">
       
        <?php 
       
        $query = "SELECT * FROM users";       
        $records_per_page=Config::get('paginator/results');
        $newquery = $paginate->paging($query,$records_per_page);
        $paginate->dataview($newquery);
        $paginate->paginglink($query,$records_per_page);  
        ?>
        
        </table>
</td>
</tr>
</table>
<p><a href='admin.php'>Back</a></p>


<?php
} else {
        echo "<p>You need to <a href='login.php'>login as admin</a> </p>";
    }