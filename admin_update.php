<?php
require_once 'app/core/init.php';
	if (Session::exists('home')) {
		echo Session::flash('home');
	}

		$user = new User;

	$db = Database::getInstance();	
	if ($user->isLoggedIn() && ($user->data()->permission == 1)) {


	$id = Input::get('id');
	$selected_user = new User($id);

	$result =  $selected_user->data();
	
		if (Input::exists()) {

			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'username'	=> array(
					'username'	=> 'Username',
					'required' 	=> true,
					'min'		=> 2,
					'max'		=> 20,
					'unique'	=> 'users'
				),
				'newPassword' => array(
					'fieldName'	=> 'New Password',
					'required' 	=> true,
					'min'		=> 6,
					'max'		=> 50
				),
				'firstname'	=> array(
					'fieldName'	=> 'First name',
					'required' 	=> true,
					'min'		=> 2,
					'max'		=> 50
				),
				'surname'	=> array(
					'fieldName'	=> 'Surname',
					'required' 	=> true,
					'min'		=> 2,
					'max'		=> 50
				),
				'permission'	=> array(
					'required'	=> true
				)		
				));

		if ($validation->passed()) {
				$salt = Hash::salt(32);
				try {
					$selected_user->update(array(
						'username' 		=> Input::get('username'),
						'password' 		=> Hash::make(Input::get('newPassword'), $salt),
						'salt' 			=> $salt,
						'firstname'		=> Input::get('firstname'),
						'surname' 		=> Input::get('surname'),
						'permission'	=> Input::get('permission')
					), $id);
					Session::flash('home','Your details have been updated');
					Redirect::to('view_users.php');
				} catch (Exception $e) {
					die($e->getMessage());
				}
			} else {
				foreach ($validation->errors() as $error) {
					echo $error, '<br>';
				}
			}
	}
} else {
		echo "<p>You need to <a href='login.php'>login as admin</a> </p>";
	}

?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="css/<?php echo DayTime::time(); ?>.css" >
</head>
<body>
	<form action="" method="post">
	<table id='tfhover' class='tftable' border='1'>
         <tr>
           <th>id</th>
           <th>Username</th>
            <th>Password</th>
            <th>First Name</th>
            <th>Surname</th>
            <th>Permission</th>
        </tr>
        <tr>
           <td><?php echo $result->id; ?></td>
           <td><?php echo $result->username; ?></td>
            <td><?php echo $result->password; ?></td>
            <td><?php echo $result->firstname; ?></td>
            <td><?php echo $result->surname; ?></td>
            <td><?php echo $result->permission; ?></td>
        </tr>
    
        <tr>
           <td><?php echo $result->id; ?></td>
           <td><input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off"/></td>
           <td><input type="text" name="newPassword" id="newPassword"/></td>
           <td><input type="text" name="firstname" id="firstname" value="<?php echo escape(Input::get('firstname')); ?>"/></td>
            <td><input type="text" name="surname" id="surname" value="<?php echo escape(Input::get('surname')); ?>"/></td>
            <td><input type="radio" name="permission" value="1">admin
				<input type="radio" name="permission" value="2">user</td>
        </tr>
    </table>    
		<br><br>
        <input type="submit" value="Update User"/>
	</form>

	<p><a href='view_users.php'>Back to users</a></p>
	<p><a href='admin.php'>Back to admin</a></p>
	

